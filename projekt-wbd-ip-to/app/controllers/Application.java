package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider;
import com.feth.play.module.pa.user.AuthUser;
import models.*;
import org.joda.time.DateTime;
import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.data.format.Formatters;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.Session;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthProvider.MyLogin;
import providers.MyUsernamePasswordAuthProvider.MySignup;
import views.html.*;
import views.html.admin.modifyflat;
import views.html.admin.modifyuser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Application extends Controller {

	static {
		Formatters.register(Block.class, new Formatters.SimpleFormatter<Block>() {

			@Override
			public Block parse(String s, Locale locale) throws ParseException {
				return Block.find.where().eq("nr", Long.parseLong(s)).findUnique();
			}

			@Override
			public String print(Block block, Locale locale) {
				return Integer.toString(block.nr);
			}
		});

		Formatters.register(Flat.class, new Formatters.SimpleFormatter<Flat>() {

			@Override
			public Flat parse(String s, Locale locale) throws ParseException {
				String[] split = s.split("/");
				Block block = Block.find.where().eq("nr", Long.parseLong(split[1])).findUnique();
				return Flat.find.where().eq("nr", Long.parseLong(split[0])).eq("block", block).findUnique();

			}

			@Override
			public String print(Flat flat, Locale locale) {
				return flat.nr + "/" + flat.block.nr;
			}
		});


	}

	public static final String FLASH_MESSAGE_KEY = "message";
	public static final String FLASH_ERROR_KEY = "error";
	public static final String USER_ROLE = "user";
	public static final String ADMIN_ROLE = "admin";
	
	public static Result index() {
		return ok(index.render());
	}

	public static User getLocalUser(final Session session) {
		final AuthUser currentAuthUser = PlayAuthenticate.getUser(session);
		final User localUser = User.findByAuthUserIdentity(currentAuthUser);
		return localUser;
	}

	@Restrict(@Group(Application.ADMIN_ROLE))
	public static Result restricted() {
		final User localUser = getLocalUser(session());
		return ok(restricted.render(localUser));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result profile() {
		final User localUser = getLocalUser(session());
		return ok(profile.render(localUser));
	}

//	@Restrict(@Group(Application.USER_ROLE))
	public static Result adminBills() {
		final User localUser = getLocalUser(session());
		java.util.List<Bill> lista2 = Bill.find.findList();
		return ok(bills.render(lista2, localUser));
	}

//	@Restrict(@Group(Application.USER_ROLE))
	public static Result bills() {
		final User localUser = getLocalUser(session());
		java.util.List<Bill> lista2 = Bill.find.where().isNull("payment_date").eq("flat", localUser.flat).findList();
		return ok(bills.render(lista2, localUser));
	}

	@Restrict(@Group(Application.USER_ROLE))
	 public static Result history() {
		final User localUser = getLocalUser(session());
		java.util.List<Bill> lista2 = Bill.find.where().eq("flat", localUser.flat).isNotNull("payment_date").findList();
		return ok(history.render(lista2, localUser));
	}

	@Restrict(@Group(Application.USER_ROLE))
	public static Result pay(final long id) {
		final User localUser = getLocalUser(session());
		Bill bill = Bill.find.byId(id);
		bill.paymentDate = new java.sql.Date(DateTime.now().getMillis());
		flash(FLASH_MESSAGE_KEY, Messages.get("playauthenticate.pay.ok", bill.id));
		return redirect(routes.Application.bills());

	}


	public static Result login() {
		return ok(login.render(MyUsernamePasswordAuthProvider.LOGIN_FORM));
	}

	public static Result doLogin() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<MyLogin> filledForm = MyUsernamePasswordAuthProvider.LOGIN_FORM
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(login.render(filledForm));
		} else {
			// Everything was filled
			return UsernamePasswordAuthProvider.handleLogin(ctx());
		}
	}

	public static Result signup() {
		return ok(signup.render(MyUsernamePasswordAuthProvider.SIGNUP_FORM));
	}

	public static Result jsRoutes() {
		return ok(
				Routes.javascriptRouter("jsRoutes",
						controllers.routes.javascript.Signup.forgotPassword()))
				.as("text/javascript");
	}

	public static Result doSignup() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<MySignup> filledForm = MyUsernamePasswordAuthProvider.SIGNUP_FORM
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(signup.render(filledForm));
		} else {
			// Everything was filled
			// do something with your part of the form before handling the user
			// signup
			return UsernamePasswordAuthProvider.handleSignup(ctx());
		}
	}

	public static String formatTimestamp(final long t) {
		return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
	}

	public static Result users(){
		List<User> user = User.find.findList();
		return ok(views.html.admin.users.render(user));
	}

	public static Result addSuperBill(){
		Form<Superbill> billForm = Form.form(Superbill.class);
		return ok(views.html.admin.addsuperbill.render(billForm));
	}


	public static Result doAddSuperBill() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<Superbill> filledForm = Form.form(Superbill.class)
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(views.html.admin.addsuperbill.render(filledForm));
		} else {
			// Everything was filled
			// do something with your part of the form before handling the user
			// signup
			Superbill bill = filledForm.get();

			bill.save();


			if(bill.howToAllocate == 1) {
				List<Flat> flist = Flat.find.findList();
				int count = flist.size();
				float perFlat = bill.amount / count;
				Bill fbill;
				for(Flat f : flist) {
					fbill = new Bill();
					fbill.amount = perFlat;
					fbill.flat = f;
					fbill.superbill = bill;
					fbill.save();
					System.out.println("kwota: " + fbill.amount + " flatid: " + fbill.flat.id);
				}

			} else if (bill.howToAllocate == 0) {
				List<Flat> flist = Flat.find.findList();
				float perFlat;
				int resAmount = Flat.sumPeople();
				float perResident = bill.amount / resAmount;
				Bill fbill;
				for(Flat f : flist) {
					perFlat = perResident * f.residentsAmount;
					fbill = new Bill();
					fbill.amount = perFlat;
					fbill.flat = f;
					fbill.superbill = bill;
					fbill.save();
				}
			}

			flash(FLASH_MESSAGE_KEY, Messages.get("playauthenticate.admin.addSuperBill.ok"));
			return redirect(routes.Application.addSuperBill());

		}
	}

	public static Result flats(){
		final User localUser = getLocalUser(session());
		java.util.List<Flat> flatList = Flat.find.findList();
		return ok(views.html.admin.flats.render(flatList, localUser));
	}

	public static Result flat(){
		final User localUser = getLocalUser(session());
		List<Flat> flatList;
		if (localUser.flat == null) {
			flatList = new ArrayList<>();
		} else {
			flatList = Collections.singletonList(localUser.flat);
		}
		return ok(views.html.admin.flats.render(flatList, localUser));
	}

	public static Result modifyFlat(long id){
		Flat flat = Flat.find.byId(id);
		Form<Flat> flatForm = Form.form(Flat.class);
		flatForm = flatForm.fill(flat);
		return ok(modifyflat.render(flatForm));
	}

	public static Result doModifyFlat(){
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		Form<Flat> filledForm = Form.form(Flat.class).bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(views.html.admin.modifyflat.render(filledForm));
		} else {
//			Flat formFlat = filledForm.get();
//			Flat localFlat = Flat.find.byId(formFlat.id);
//			localFlat.block = formFlat.block;
//			localFlat.metrage = formFlat.metrage;
//			localFlat.nr = formFlat.nr;
//			localFlat.residentsAmount = formFlat.residentsAmount;
			Flat localFlat = filledForm.get();
			localFlat.update();
			flash(FLASH_MESSAGE_KEY, Messages.get("playauthenticate.admin.modifyflat.ok", localFlat.id));
			return redirect(routes.Application.flats());
		}
	}

	public static Result superBills(){
		final User localUser = getLocalUser(session());
		java.util.List<Superbill> superbillsList = Superbill.find.findList();
		return ok(views.html.admin.superbills.render(superbillsList, localUser));
	}

	public static Result modifyUser(long i){ //get z linka
		User user = User.find.byId(i);
		Form<User> userForm = Form.form(User.class);
		userForm = userForm.fill(user);
		return ok(modifyuser.render(userForm));
	}

	public static Result doModifyUser(){
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		DynamicForm filledForm = new DynamicForm()
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(views.html.admin.modifyuser.render(null));
		} else {
			// Everything was filled
			// do something with your part of the form before handling the user
			// signup
			User user = User.find.byId(Long.parseLong(filledForm.get("id")));
			user.id = Long.parseLong(filledForm.get("id"));
			String[] split = filledForm.get("flat").split("/");
			Block block = Block.find.where().eq("nr", Long.parseLong(split[1])).findUnique();
			Flat flat =  Flat.find.where().eq("nr", Long.parseLong(split[0])).eq("block", block).findUnique();
			user.name = filledForm.get("name");
			user.email = filledForm.get("email");
			user.flat = flat;
			user.update();
			flash(FLASH_MESSAGE_KEY, Messages.get("playauthenticate.admin.modifyuser.ok", user.id));
			return redirect(routes.Application.users());

		}
	}

	public static Result adminize() {
		User user = getLocalUser(session());
		SecurityRole adminRole = SecurityRole.findByRoleName("admin");
		//SecurityRole userRole = SecurityRole.findByRoleName("user");
		user.roles.add(adminRole);
		//user.roles.remove(userRole);
		user.save();
		//user.update();
		flash(FLASH_MESSAGE_KEY, "Masz super moce");
		return redirect(routes.Application.index());
	}
}