package controllers;

import com.avaje.ebean.ExpressionList;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import views.html.index;

import java.util.List;

/**
 * Created by dexior on 1/21/15.
 */
public class Activate extends Controller {
    public static Result activate() {
        List<User> users = User.find.where().eq("active", false).findList();
        for (User user : users) {
            user.active = true;
            user.save();
        }

        return ok(index.render());
    }
}
