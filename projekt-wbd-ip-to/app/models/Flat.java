package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by Maciej on 2015-01-19.
 */
@Entity
@Table (name = "flats")
public class Flat extends Model{

    public static final Finder<Long, Flat> find = new Finder<Long, Flat>(
            Long.class, Flat.class);

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false)
    public int nr;

    @Column(nullable = false)
    public float metrage;

    @Column(nullable = false)
    public int residentsAmount;

    @Valid
    @ManyToOne
    public Block block;

    @OneToMany(cascade = CascadeType.ALL)
    public Bill bill;

    public static int sumPeople() {
        String sql = "select sum(residents_amount) from flats";
        SqlRow sqlRow = Ebean.createSqlQuery(sql).findUnique();
        int sum = sqlRow.getInteger("sum(residents_amount)");
        return sum;
    }


}
