package models;

import javax.persistence.*;

import play.data.format.Formats;
import play.db.ebean.Model;

import java.util.Date;
import java.util.List;

/**
 * Created by Maciej on 2015-01-20.
 */

@Entity
@Table(name = "superbills")
public class Superbill extends Model{

    public static final Finder<Long, Superbill> find = new Finder<Long, Superbill>(
            Long.class, Superbill.class);

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false)
    public float amount;

    @Column(nullable = false)
    @Formats.DateTime(pattern="yyyy-MM-dd")
    public Date prompt;

    @Column(nullable = false)
    public String description;

    @OneToMany(cascade = CascadeType.ALL)
    public List<Bill> bills;

    public int howToAllocate;
}
