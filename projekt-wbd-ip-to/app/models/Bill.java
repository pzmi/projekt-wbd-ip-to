package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Maciej on 2015-01-19.
 */
@Entity
@Table(name = "bills")
public class Bill extends Model {

    public static final Finder<Long, Bill> find = new Finder<Long, Bill>(
            Long.class, Bill.class);

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false)
    public float amount;

    @Column(nullable = true)
    public Date paymentDate;

    @ManyToOne
    public Superbill superbill;

    @ManyToOne
    public Flat flat;

}