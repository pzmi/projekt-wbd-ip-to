package models;

import javax.persistence.*;

import play.db.ebean.Model;

import java.util.List;

/**
 * Created by Maciej on 2015-01-19.
 */

@Entity
@Table(name = "blocks")
public class Block  extends Model{

    public static final Finder<Long, Block> find = new Finder<Long, Block>(
            Long.class, Block.class);
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false)
    public int nr;

    @Column(nullable = false)
    public String street;

    @OneToMany(cascade = CascadeType.ALL)
    List<Flat> flats;

}
