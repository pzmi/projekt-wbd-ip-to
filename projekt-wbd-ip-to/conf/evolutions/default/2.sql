# --- !Ups

insert into blocks (id, nr, street) values (0, 1, 'Warszawska');

insert into flats (id, block_id, nr, metrage, residents_amount) values (1, 0, 37, 73.07, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (2, 0, 58, 61.73, 3);
insert into flats (id, block_id, nr, metrage, residents_amount) values (3, 0, 33, 76.27, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (4, 0, 12, 74.2, 4);
insert into flats (id, block_id, nr, metrage, residents_amount) values (5, 0, 87, 73.48, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (6, 0, 97, 59.34, 3);
insert into flats (id, block_id, nr, metrage, residents_amount) values (7, 0, 49, 68.51, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (8, 0, 53, 76.83, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (9, 0, 45, 66.44, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (10, 0, 9, 67.37, 3);
insert into flats (id, block_id, nr, metrage, residents_amount) values (11, 0, 14, 63.37, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (12, 0, 74, 64.96, 3);
insert into flats (id, block_id, nr, metrage, residents_amount) values (13, 0, 93, 73.75, 5);
insert into flats (id, block_id, nr, metrage, residents_amount) values (14, 0, 46, 77.41, 3);
insert into flats (id, block_id, nr, metrage, residents_amount) values (15, 0, 90, 57.49, 3);
insert into flats (id, block_id, nr, metrage, residents_amount) values (16, 0, 92, 77.31, 5);


# --- !Downs

DELETE FROM blocks;
DELETE FROM flats;