# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table bills (
  id                        bigint auto_increment not null,
  amount                    float not null,
  payment_date              date,
  superbill_id              bigint,
  flat_id                   bigint,
  constraint pk_bills primary key (id))
;

create table blocks (
  id                        bigint auto_increment not null,
  nr                        integer not null,
  street                    varchar(255) not null,
  constraint pk_blocks primary key (id))
;

create table flats (
  id                        bigint auto_increment not null,
  nr                        integer not null,
  metrage                   float not null,
  residents_amount          integer not null,
  block_id                  bigint,
  constraint pk_flats primary key (id))
;

create table linked_account (
  id                        bigint not null,
  user_id                   bigint,
  provider_user_id          varchar(255),
  provider_key              varchar(255),
  constraint pk_linked_account primary key (id))
;

create table security_role (
  id                        bigint not null,
  role_name                 varchar(255),
  constraint pk_security_role primary key (id))
;

create table superbills (
  id                        bigint auto_increment not null,
  amount                    float not null,
  prompt                    timestamp not null,
  description               varchar(255) not null,
  how_to_allocate           integer,
  constraint pk_superbills primary key (id))
;

create table token_action (
  id                        bigint not null,
  token                     varchar(255),
  target_user_id            bigint,
  type                      varchar(2),
  created                   timestamp,
  expires                   timestamp,
  constraint ck_token_action_type check (type in ('PR','EV')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id))
;

create table users (
  id                        bigint not null,
  email                     varchar(255),
  name                      varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  last_login                timestamp,
  active                    boolean,
  email_validated           boolean,
  flat_id                   bigint,
  constraint uq_users_email unique (email),
  constraint pk_users primary key (id))
;

create table user_permission (
  id                        bigint not null,
  value                     varchar(255),
  constraint pk_user_permission primary key (id))
;


create table users_security_role (
  users_id                       bigint not null,
  security_role_id               bigint not null,
  constraint pk_users_security_role primary key (users_id, security_role_id))
;

create table users_user_permission (
  users_id                       bigint not null,
  user_permission_id             bigint not null,
  constraint pk_users_user_permission primary key (users_id, user_permission_id))
;
create sequence linked_account_seq;

create sequence security_role_seq;

create sequence token_action_seq;

create sequence users_seq;

create sequence user_permission_seq;

alter table bills add constraint fk_bills_superbill_1 foreign key (superbill_id) references superbills (id) on delete restrict on update restrict;
create index ix_bills_superbill_1 on bills (superbill_id);
alter table bills add constraint fk_bills_flat_2 foreign key (flat_id) references flats (id) on delete restrict on update restrict;
create index ix_bills_flat_2 on bills (flat_id);
alter table flats add constraint fk_flats_block_3 foreign key (block_id) references blocks (id) on delete restrict on update restrict;
create index ix_flats_block_3 on flats (block_id);
alter table linked_account add constraint fk_linked_account_user_4 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_linked_account_user_4 on linked_account (user_id);
alter table token_action add constraint fk_token_action_targetUser_5 foreign key (target_user_id) references users (id) on delete restrict on update restrict;
create index ix_token_action_targetUser_5 on token_action (target_user_id);
alter table users add constraint fk_users_flat_6 foreign key (flat_id) references flats (id) on delete restrict on update restrict;
create index ix_users_flat_6 on users (flat_id);



alter table users_security_role add constraint fk_users_security_role_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_securi_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_user_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_user_permission add constraint fk_users_user_permission_user_02 foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists bills;

drop table if exists blocks;

drop table if exists flats;

drop table if exists linked_account;

drop table if exists security_role;

drop table if exists superbills;

drop table if exists token_action;

drop table if exists users;

drop table if exists users_security_role;

drop table if exists users_user_permission;

drop table if exists user_permission;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists linked_account_seq;

drop sequence if exists security_role_seq;

drop sequence if exists token_action_seq;

drop sequence if exists users_seq;

drop sequence if exists user_permission_seq;

